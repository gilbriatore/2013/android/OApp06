package br.up.edu.oapp06;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    /**
     * App 6. Desenvolva um aplicativo que receba
     * três notas de um aluno e retorne a sua média
     * aritmética.
     * Exemplo:
     * nota1 = 10.0,
     * nota2 = 5.5,
     * nota3 = 8.0.
     * Média: 7.83
     */
    public void calcular(View v){

        //Pegar as caixas de texto;
        EditText cxNota1 = (EditText) findViewById(R.id.txtNota1);
        EditText cxNota2 = (EditText) findViewById(R.id.txtNota2);
        EditText cxNota3 = (EditText) findViewById(R.id.txtNota3);
        EditText cxMedia = (EditText) findViewById(R.id.txtMedia);

        //Pegar os textos das caixas;
        String txtNota1 = cxNota1.getText().toString();
        String txtNota2 = cxNota2.getText().toString();
        String txtNota3 = cxNota3.getText().toString();

        //Converter para números;
        double nota1 = Double.parseDouble(txtNota1);
        double nota2 = Double.parseDouble(txtNota2);
        double nota3 = Double.parseDouble(txtNota3);

        //Calcular a média aritmética;
        double media = (nota1 + nota2 + nota3) / 3;

        //Mostrar para o usuário;
        cxMedia.setText(String.valueOf(media));
    }

}
